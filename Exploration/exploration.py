import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

df_users = pd.read_csv("user.csv")
df_jobs = pd.read_csv("job_desc.csv")

df_joined = pd.merge(df_users, df_jobs, on='user_id')

print(df_joined.columns)
print(df_joined['has_applied'].describe())

# 57.6% of people applied


grid = sns.PairGrid(df_joined, vars=["company", "salary"], hue="has_applied")
grid.map(plt.scatter)
grid.add_legend();
grid.savefig("grid.png")

#for variable in df_joined.columns:
#    data = pd.concat([df_joined['has_applied'], df_joined[variable]], axis=1)
#    data.plot.scatter(x=variable, y='has_applied', ylim=(0,800000));

