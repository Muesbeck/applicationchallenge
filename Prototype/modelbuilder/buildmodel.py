import re
import pickle
import pandas as pd
import xgboost as xgb
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import LabelEncoder

def build_model(X, y):
	modelClassifier = xgb.XGBClassifier( 
		learning_rate = 0.1,
		n_estimators=140,
		max_depth=6,
		min_child_weight=8,
		gamma=0,
		subsample=0.9,
		colsample_bytree=0.9,
		objective = 'binary:logistic',
		nthread=4,
		scale_pos_weight=1,
		reg_alpha = 0.1,
		seed=23
		)
	model = modelClassifier.fit(X, y)
	print("Model created!")
	return model

def split_data(dataframe):
	df_X = dataframe.loc[:, dataframe.columns != 'has_applied']
	df_X = df_X.loc[:, df_X.columns != 'job_title_full']
	df_y = dataframe.loc[:, 'has_applied']

	return df_X, df_y

# This method could be replaced to get data from other sources
def load_data():
	df_users = pd.read_csv("data/user.csv")
	df_jobs = pd.read_csv("data/job_desc.csv")
	df_joined = pd.merge(df_users, df_jobs, on='user_id')

	return df_joined

def clean_and_add_features (dataframe):
	# get rid of user ids, they only matter in overfit models
	new_df = dataframe.loc[:, dataframe.columns != 'user_id']

	# encode variables
	onehot_encoder = OneHotEncoder()
	enc_df = pd.DataFrame(onehot_encoder.fit_transform(new_df[['company']]).toarray())

	# add encoded columns to dataframe
	new_df = new_df.join(enc_df)
	# don't need company anymore
	new_df = new_df.loc[:, new_df.columns != 'company']
	new_df.rename(columns = {0 : 'company_a', 1 : 'company_b', 2 : 'company_c', 3 : 'company_d', 4 : 'company_e', 5 : 'company_f', 6 : 'company_g', 7 : 'company_h', 8 : 'company_i'}, inplace = True)

	# A bunch of new features
	labelencoder = LabelEncoder()
	new_df['has_salary'] = ~new_df['salary'].isna()
	new_df['has_salary'] = labelencoder.fit_transform(new_df['has_salary'])
	
	new_df['has_mfd'] = new_df['job_title_full'].apply(lambda x: "m/f/d" in x.lower())
	new_df['has_mfd'] = labelencoder.fit_transform(new_df['has_mfd'])
	
	new_df['char_length_title'] = new_df['job_title_full'].apply(lambda x: len(x))
	new_df['count_spaces'] = new_df['job_title_full'].apply(lambda x: x.count(" "))
	new_df['count_periods'] = new_df['job_title_full'].apply(lambda x: x.count("."))
	new_df['count_exclamation'] = new_df['job_title_full'].apply(lambda x: x.count("!"))
	new_df['count_question'] = new_df['job_title_full'].apply(lambda x: x.count("?"))
	new_df['avg_word_length'] = new_df['job_title_full'].apply(lambda x: len(x) / (x.count(" ") +1))
	new_df['avg_sentence_length'] = new_df['job_title_full'].apply(lambda x: x.count(" ") / (x.count(".") +1))
	new_df['count_digits'] = new_df['job_title_full'].apply(lambda x: len(re.findall("d", x)))
	new_df['count_slashes'] = new_df['job_title_full'].apply(lambda x: x.count("/"))
	new_df['count_paren'] = new_df['job_title_full'].apply(lambda x: x.count("("))
	
	# For a different algorithm such as regression, dealing with 
	# missing data (i.e. imputation or dropping data) would be necessary but trees do well with missing data
	#new_df = impute_simple(new_df)

	return new_df

def vectorize_titles(dataframe):
	# Vectorize the job titles
	vec = TfidfVectorizer(ngram_range=(1, 3), max_features=50000, min_df=2)
	vectorized = vec.fit_transform(dataframe['job_title_full'])
	df_vectorized = pd.DataFrame(vectorized.todense(), columns= vec.get_feature_names())

	df_combined = df_vectorized.join(dataframe.loc[:, dataframe.columns != 'job_title_full'])
	return df_combined


def main():
	fresh_data = load_data()
	clean_data = clean_and_add_features(fresh_data)
	vectorized_data = vectorize_titles(clean_data)
	X, y = split_data(vectorized_data)
	model = build_model(X, y)
	pickle.dump(model, open("/model/model.pickle.dat", "wb"))
	print("Done writing model")

if __name__ == "__main__":
    main()

